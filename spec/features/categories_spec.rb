require 'rails_helper'

RSpec.feature "categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy, taxons: [taxon, taxon2]) }
  let!(:taxonomy2) { create(:taxonomy, taxons: [taxon3]) }
  let!(:taxon) { create(:taxon, products: [product]) }
  let!(:taxon2) { create(:taxon, parent_id: taxon.id, products: [product]) }
  let!(:taxon3) { create(:taxon, products: [product2]) }
  let(:product) { create(:product) }
  let(:product2) { create(:product) }

  before { visit potepan_category_path(taxon.id) }

  scenario "show template show information of taxon" do
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "show template do not show information of another taxon product" do
    expect(page).not_to have_content product2.name
  end

  scenario "check link in show template" do
    click_link taxon2.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    click_link product.name
    expect(page).to have_content product.description
  end
end
