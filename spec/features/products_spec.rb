require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon, taxon2], name: "p1") }
  let(:product2) { create(:product, name: "p2") }
  let(:product3) { create(:product, name: "p3") }
  let(:product4) { create(:product, name: "p4") }
  let(:taxon) { create(:taxon, products: [product2], name: "t1") }
  let(:taxon2) { create(:taxon, products: [product3], name: "t2") }
  let(:taxon3) { create(:taxon, products: [product4], name: "t3") }

  before { visit potepan_product_path(product.id) }

  scenario "single product template show information of product" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  context "product with option_type" do
    scenario "single product template show information of option_type" do
      product.option_types.each do |option_type|
        expect(page).to have_select(options: option_type.option_values.presentation)
      end
    end
  end

  scenario "single product template show information of related_product" do
    expect(page).to have_content taxon.name
    expect(page).to have_content product2.name
    expect(page).to have_content product2.display_price
    expect(page).to have_content taxon2.name
    expect(page).to have_content product3.name
    expect(page).to have_content product3.display_price
    click_link product2.name
    expect(page).to have_content product2.description
  end

  scenario "single product template do not show information of not related_product" do
    expect(page).not_to have_content taxon3.name
    expect(page).not_to have_content product4.name
  end
end
