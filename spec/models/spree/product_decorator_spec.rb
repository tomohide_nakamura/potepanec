require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#option_type_presentation" do
    let(:size) { create(:option_type, presentation: 'Size') }
    let(:color) { create(:option_type, presentation: 'Color') }
    let(:product) { create(:product, option_types: [size, color]) }

    it "returns array of product.option_type.presentation" do
      expect(product.option_type_presentation).to eq ['Size', 'Color']
    end
  end

  describe "#option_value_presentation" do
    let(:size) { create(:option_type, presentation: 'Size') }
    let(:color) { create(:option_type, presentation: 'Color') }
    let(:product) { create(:product, option_types: [size, color]) }
    let(:size_small) { create(:option_value, presentation: 'S', option_type: size) }
    let(:size_medium) { create(:option_value, presentation: 'M', option_type: size) }
    let(:size_large) { create(:option_value, presentation: 'L', option_type: size) }
    let(:color_red) { create(:option_value, presentation: 'Red', option_type: color) }
    let(:color_green) { create(:option_value, presentation: 'Green', option_type: color) }
    let!(:variant1) { create(:variant, product: product, option_values: [size_small, size_medium, size_large]) }
    let!(:variant2) { create(:variant, product: product, option_values: [color_red, color_green]) }

    it "returns array of product.option_value.presentation(Size)" do
      expect(product.option_value_presentation("Size")).to eq ['S', 'M', 'L']
    end

    it "returns array of product.option_value.presentation(Color)" do
      expect(product.option_value_presentation("Color")).to eq ['Red', 'Green']
    end
  end
end
