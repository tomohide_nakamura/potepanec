require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomies) { create_list(:taxonomy, 3) }
    let(:products) { create_list(:product, 3) }

    before { get :show, params: { taxon_id: taxon.id } }

    it "responds successfully" do
      expect(response).to be_successful
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
