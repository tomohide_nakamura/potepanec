require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product, taxons: [taxon, taxon2]) }
    let(:product2) { create(:product) }
    let(:product3) { create(:product) }
    let(:taxon) { create(:taxon, products: [product2], name: :tax1) }
    let(:taxon2) { create(:taxon, products: [product3], name: :tax2) }

    before { get :show, params: { id: product.id } }

    it "responds successfully" do
      expect(response).to be_successful
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
