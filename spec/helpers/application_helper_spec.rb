require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe "#full_title" do
    context 'page_title is blank' do
      it { expect(full_title("")).to eq "Potepanec" }
    end

    context 'page_title is not blank' do
      it { expect(full_title("Index")).to eq "Index | Potepanec" }
    end
  end

  describe "#set_active" do
    before do
      provide(:index, true)
    end

    context 'args is :index' do
      it { expect(set_active(:index)).to eq 'active' }
    end

    context 'args is :index, :single_product' do
      it { expect(set_active(:index, :single_product)).to eq 'active' }
    end

    context 'args is :single_product' do
      it { expect(set_active(:single_product)).to eq '' }
    end
  end
end
