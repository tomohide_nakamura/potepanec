Spree::Product.class_eval do
  def option_type_presentation
    option_types.map(&:presentation)
  end

  def option_value_presentation(option_type_presentation)
    option_types.find_by(presentation: option_type_presentation).option_values.map(&:presentation)
  end
end
