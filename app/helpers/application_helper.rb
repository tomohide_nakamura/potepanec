module ApplicationHelper
  def full_title(page_title)
    base_title = "Potepanec"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def set_active(*args)
    args.each do |arg|
      return "active" if content_for?(arg)
    end
    ""
  end
end
