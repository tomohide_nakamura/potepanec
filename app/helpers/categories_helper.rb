module CategoriesHelper
  def taxons_list(root_taxon)
    if root_taxon.root?
      taxons_list_content(root_taxon)
    else
      content_tag :ul, id: root_taxon.id, class: "collapse collapseItem" do
        taxons_list_content(root_taxon)
      end
    end
  end

  def taxons_list_content(root_taxon)
    taxons = root_taxon.children.map do |taxon|
      triangle = taxon.parent.root? ? nil : "fa fa-caret-right"
      hidden = !taxon.parent.root?
      content_tag :li do
        if taxon.leaf?
          link_to(potepan_category_path(taxon.id)) do
            content_tag(:i, nil, class: triangle, "aria-hidden" => hidden) +
            "#{taxon.name} (#{taxon.all_products.count})"
          end
        else
          link_to("javascript:;", { "data-toggle" => "collapse", "data-target" => "##{taxon.id}" }) do
            content_tag(:i, nil, class: ["fa fa-plus", triangle], "aria-hidden" => hidden) +
            "#{taxon.name} (#{taxon.all_products.count})"
          end + taxons_list(taxon)
        end
      end
    end
    safe_join(taxons, "\n")
  end
end
