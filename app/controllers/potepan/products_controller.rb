class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_RELATED_PRODUCTS_BY_THE_TAXON = 4

  def show
    @product = Spree::Product.find(params[:id])
    @products = @product.taxons.map do |taxon|
      [
        taxon.name.to_sym,
        taxon.products.where.not(id: @product.id).
          includes(master: [:default_price, :images]).
          sample(MAX_NUMBER_OF_RELATED_PRODUCTS_BY_THE_TAXON),
      ]
    end.to_h
  end
end
