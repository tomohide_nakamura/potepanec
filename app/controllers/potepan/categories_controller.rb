class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @taxonomies = Spree::Taxonomy.eager_load(root: { children: :children })
    @products = @taxon.all_products.includes(master: [:default_price, :images])
  end
end
